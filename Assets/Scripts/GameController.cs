using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameController : MonoBehaviour
{
	[SerializeField] private Star _star;
	[SerializeField] private int _starMaxCount = 15;
	[SerializeField] private float _spawnRightCorner = 6.5f;
	[SerializeField] private float _spawnLeftCorner = -5.5f;
	[SerializeField] private float _spawnHeight = 6f;
	[SerializeField] private float _minPushForce = 500f;
	[SerializeField] private float _maxPushForce = 1000f;
	private int _totalScore;
	private Text _scoreText;
	private Queue<Star> _starsPool;
	private IEnumerator _starCycle;

	public float MinPushForce { get { return _minPushForce; } }
	public float MaxPushForce { get { return _maxPushForce; } }

	public void AddScore() 
	{ 
		_totalScore++;
		_scoreText.text = "Score: " + _totalScore;
	}

	public int GetScore()
    {
		return _totalScore;
    }

	public void ResetGame()
    {
		_totalScore = 0;
		StopCoroutine(_starCycle);
		for (int i = 0; i < _starMaxCount; i++)
		{
			Star prefab = _starsPool.Dequeue();
			prefab.gameObject.SetActive(false);
			_starsPool.Enqueue(prefab);
		}
	}

	void Start()
    {
        _totalScore = 0;
		_scoreText = GameObject.Find("CurrentScore").GetComponent<Text>();
		_scoreText.text = "Score: " + _totalScore;

		_starsPool = new Queue<Star>();
		for(int i = 0; i < _starMaxCount; i++)
        {
			Star prefab = Instantiate(_star);
			prefab.SetController(this);
			prefab.gameObject.SetActive(false);
			_starsPool.Enqueue(prefab);
        }
    }
    
	public void LaunchGame()
    {
		_starCycle = StarSpawnCycle();
		StartCoroutine(_starCycle);
	}

	private IEnumerator StarSpawnCycle()
	{
		while(true)
		{
			Vector2 starPosition = new Vector2(Random.Range(_spawnLeftCorner, _spawnRightCorner), _spawnHeight);
			float pushForce = Random.Range(_minPushForce, _maxPushForce);
			SpawnStar(starPosition, transform.rotation,  pushForce);
			yield return new WaitForSeconds(Random.Range(0f, 1f));
		}
	}

	public void SpawnStar(Vector2 position, Quaternion rotation, float pushForce)
    {
		Star prefab = _starsPool.Dequeue();

		prefab.gameObject.SetActive(true);
		prefab.transform.position = position;
		prefab.transform.rotation = rotation;
		prefab.GetComponent<Star>().StarPush(pushForce);

		_starsPool.Enqueue(prefab);
	}

}
