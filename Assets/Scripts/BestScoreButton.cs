using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class BestScoreButton : MonoBehaviour
{
    [SerializeField] private Text _text;
    [SerializeField] private StartButton _startButton;
    [SerializeField] private ExitButton _exitButton;
    [SerializeField] private DataManager _dataManager;
    [SerializeField] private Button _button;
    private bool _active;

    private void Start()
    {
        _active = false;
        _button.onClick.AddListener(OnButtonClick);
    }

    public void OnButtonClick()
    {
        _text.text = _active ? "BEST" : _dataManager.GetHightScore().ToString();
        _startButton.gameObject.SetActive(_active);
        _exitButton.gameObject.SetActive(_active);
        _active = _active ? false : true;
    }
}
