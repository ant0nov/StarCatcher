using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ExitButton : MonoBehaviour
{
    [SerializeField] private DataManager _dataManager;
    [SerializeField] private Button _button;
    void Start()
    {
       _button.onClick.AddListener(OnButtonClick);
    }

    private void OnButtonClick()
    {
        _dataManager.SaveData();
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
}
