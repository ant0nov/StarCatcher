using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Star : MonoBehaviour
{
	[SerializeField] private float _minRotation = -100f;
	[SerializeField] private float _maxRotation = 100f;
	[SerializeField] private Rigidbody2D _rb;
	private GameController _controller;
	private float _rotation;
	
	private void Start()
    {
		_rotation = Random.Range(0, 2) == 0 ? _minRotation : _maxRotation;
	}

	public void SetController(GameController gameController)
    {
		_controller = gameController;
    }
	public void StarPush(float forse)
    {
		_rb.velocity = Vector2.zero;
		_rb.AddForce(new Vector2(0, -forse));
	}

    void OnMouseDown()
    {
		if (Time.deltaTime > 0f)
        {
			_controller.AddScore();
			gameObject.SetActive(false);
		}
    }
}