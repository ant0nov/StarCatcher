using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataManager : MonoBehaviour
{
    private int _hightScore;
    private void Start()
    {
        LoadData();
    }

    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + "/HighScore.dat");
        bf.Serialize(file, _hightScore);
        file.Close();
    }

    public void UpdateScore(int newScore)
    {
        if (newScore > _hightScore)
            _hightScore = newScore;
    }

    public int GetHightScore()
    {
        return _hightScore;
    }

    private void LoadData()
    {
        if(File.Exists(Application.persistentDataPath + "/HighScore.dat"))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + "/HighScore.dat", FileMode.Open);
            _hightScore = (int)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            _hightScore = 0;
        }
    }
}
