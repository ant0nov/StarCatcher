using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class StartButton : MonoBehaviour
{
    [SerializeField] private BestScoreButton _bestScoreButton;
    [SerializeField] private ExitButton _exitButton;
    [SerializeField] private Menu _menu;
    [SerializeField] private Timer _timer;
    [SerializeField] private Button _button;
    void Start()
    {
        _button.onClick.AddListener(OnButtonClick);
    }

    public void OnButtonClick()
    {
        _bestScoreButton.gameObject.SetActive(false);
        _exitButton.gameObject.SetActive(false);
        _menu.UnActivate();
        _timer.TimerStart();
        gameObject.SetActive(false);
    }
}
