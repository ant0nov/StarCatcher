using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    [SerializeField] private GameObject[] _buttons;
    private bool _active;

    public void Start()
    {
        _active = true;
    }
    public void UnActivate()
    {
        _active = false;
        Time.timeScale = 1f;
    }

    public void Activate()
    {
        if (_buttons != null)
        {
            for (int i = 0; i < _buttons.Length; i++)
            {
                _buttons[i].SetActive(true);
            }
        }
        Time.timeScale = 0f;
        _active = true;
    }
    
    public void Update()
    {
		if (Input.GetKey(KeyCode.Escape) && !_active)
		{
            Activate();
        }
	}

}
