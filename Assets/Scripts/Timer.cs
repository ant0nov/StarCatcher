using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
    [SerializeField] private int _roundTime = 10;
    [SerializeField] private GameController _controller;
    [SerializeField] private Menu _menu;
    [SerializeField] private Text _currentTime;
    [SerializeField] private DataManager _dataManager;

    public void TimerStart()
    {
        StartCoroutine(TimerCycle());
    }
    private IEnumerator TimerCycle()
    {
        _controller.LaunchGame();
        int roundTime = _roundTime;
        _currentTime.text = "Time: " + roundTime;
        while (roundTime > 0)
        {
            yield return new WaitForSeconds(1f);
            roundTime--;
            _currentTime.text = "Time: " + roundTime;
        }
        _dataManager.UpdateScore(_controller.GetScore());
        _controller.ResetGame();
        _menu.Activate();
    }
}
